﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyEasyCalculator
{
    public class Action
    {
        private string[] twoOperands = { "+", "-", "*", "/", "^" };
        private string[] oneOperand = { "√" };
        private string[] zeroOperand = { "(", ")" };
        private int _priority;
        private int _operands;
        public int operands
        {
            get { return _operands; }
        }
        public int priority
        {
            get { return _priority; }
            set { _priority = value;}
        }
        private string _name;
        public Action(string s)
        {
            _name = s;
            if (twoOperands.Contains(s))
            {
                _operands = 2;
            }
            else if (oneOperand.Contains(s))
            {
                _operands = 1;
            }
            switch (s)
            {
                case "(":
                case ")":
                    priority = 5;
                    break;
                case "+":
                case "-":
                    priority = 1;
                    break;
                case "*":
                case "/":
                    priority = 2;
                    break;
                case "^":
                case "√":
                    priority = 3;
                    break;
                default:
                    priority = 4;
                    break;
            }
        }
    }
}
