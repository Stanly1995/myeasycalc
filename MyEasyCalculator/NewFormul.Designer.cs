﻿namespace MyEasyCalculator
{
    partial class NewFormul
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameLabel = new System.Windows.Forms.Label();
            this.OperandsLabel = new System.Windows.Forms.Label();
            this.FormulLabel = new System.Windows.Forms.Label();
            this.NameFormul = new System.Windows.Forms.TextBox();
            this.Operands = new System.Windows.Forms.TextBox();
            this.Formul = new System.Windows.Forms.TextBox();
            this.New = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.Delete = new System.Windows.Forms.Button();
            this.Change = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(22, 19);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(72, 17);
            this.NameLabel.TabIndex = 0;
            this.NameLabel.Text = "Название";
            // 
            // OperandsLabel
            // 
            this.OperandsLabel.AutoSize = true;
            this.OperandsLabel.Location = new System.Drawing.Point(22, 52);
            this.OperandsLabel.Name = "OperandsLabel";
            this.OperandsLabel.Size = new System.Drawing.Size(161, 17);
            this.OperandsLabel.TabIndex = 1;
            this.OperandsLabel.Text = "Количество операндов";
            // 
            // FormulLabel
            // 
            this.FormulLabel.AutoSize = true;
            this.FormulLabel.Location = new System.Drawing.Point(22, 89);
            this.FormulLabel.Name = "FormulLabel";
            this.FormulLabel.Size = new System.Drawing.Size(69, 17);
            this.FormulLabel.TabIndex = 2;
            this.FormulLabel.Text = "Формула";
            // 
            // NameFormul
            // 
            this.NameFormul.Location = new System.Drawing.Point(236, 13);
            this.NameFormul.Name = "NameFormul";
            this.NameFormul.Size = new System.Drawing.Size(219, 22);
            this.NameFormul.TabIndex = 3;
            // 
            // Operands
            // 
            this.Operands.Location = new System.Drawing.Point(236, 47);
            this.Operands.Name = "Operands";
            this.Operands.Size = new System.Drawing.Size(219, 22);
            this.Operands.TabIndex = 4;
            // 
            // Formul
            // 
            this.Formul.Location = new System.Drawing.Point(236, 86);
            this.Formul.Name = "Formul";
            this.Formul.Size = new System.Drawing.Size(219, 22);
            this.Formul.TabIndex = 5;
            // 
            // New
            // 
            this.New.Location = new System.Drawing.Point(25, 140);
            this.New.Name = "New";
            this.New.Size = new System.Drawing.Size(199, 23);
            this.New.TabIndex = 6;
            this.New.Text = "Добавить";
            this.New.UseVisualStyleBackColor = true;
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(256, 140);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(199, 23);
            this.Back.TabIndex = 7;
            this.Back.Text = "Отмена";
            this.Back.UseVisualStyleBackColor = true;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(25, 224);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(430, 160);
            this.treeView1.TabIndex = 8;
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(256, 180);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(199, 23);
            this.Delete.TabIndex = 10;
            this.Delete.Text = "Удалить";
            this.Delete.UseVisualStyleBackColor = true;
            // 
            // Change
            // 
            this.Change.Location = new System.Drawing.Point(25, 180);
            this.Change.Name = "Change";
            this.Change.Size = new System.Drawing.Size(199, 23);
            this.Change.TabIndex = 9;
            this.Change.Text = "Изменить";
            this.Change.UseVisualStyleBackColor = true;
            // 
            // NewFormul
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 396);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Change);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.New);
            this.Controls.Add(this.Formul);
            this.Controls.Add(this.Operands);
            this.Controls.Add(this.NameFormul);
            this.Controls.Add(this.FormulLabel);
            this.Controls.Add(this.OperandsLabel);
            this.Controls.Add(this.NameLabel);
            this.Name = "NewFormul";
            this.Text = "NewFormul";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label OperandsLabel;
        private System.Windows.Forms.Label FormulLabel;
        private System.Windows.Forms.TextBox NameFormul;
        private System.Windows.Forms.TextBox Operands;
        private System.Windows.Forms.TextBox Formul;
        private System.Windows.Forms.Button New;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Change;
    }
}