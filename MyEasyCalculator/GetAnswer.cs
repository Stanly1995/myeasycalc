﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyEasyCalculator
{
    static class GetAnswer
    {
        static List<string> myChars = new List<string>() { "(", ")", "*", "/", "+", "-", "^", "√" };
        static Dictionary<string, Action> myAct = new Dictionary<string, Action>()
        {
            {"(", new Action("(") },
            {")", new Action(")") },
            {"+", new Action("+") },
            {"-", new Action("-") },
            {"*", new Action("*") },
            {"/", new Action("/") },
            {"^", new Action("^") },
            {"√", new Action("√") },
        };
        static Stack<string> myActions = new Stack<string>();
        static Stack<double> myNums = new Stack<double>();
        private static string ToPoland(string s)
        {
            string[] firstStep = (s.Split('@'));
            List<string> secondStep = firstStep.ToList<string>();
            List<string> thirdStep = new List<string>(); ;
            for (int i = 0; i < secondStep.Count; i++)
            {
                if (myChars.Contains(secondStep[i]))
                    if (myActions.Count == 0)
                    {
                        myActions.Push(secondStep[i]);
                    }
                    else
                    {
                        if (secondStep[i].CompareTo(")") == 0)
                        {
                            while (myActions.Peek().CompareTo("(") != 0)
                            {
                                thirdStep.Add(myActions.Pop());
                            }
                            myActions.Pop();
                        }
                        else
                        {
                            if (myActions.Peek().CompareTo("(") == 0)
                            {
                                myActions.Push(secondStep[i]);
                            }
                            else
                            {
                                if (myAct[secondStep[i]].priority <= myAct[myActions.Peek()].priority)
                                {
                                    thirdStep.Add(myActions.Pop());
                                    myActions.Push(secondStep[i]);
                                }
                                else
                                {
                                    myActions.Push(secondStep[i]);
                                }
                            }
                        }
                    }
                else
                {
                    thirdStep.Add(secondStep[i]);
                }
               
            }
            while (myActions.Count > 0)
            {
                thirdStep.Add(myActions.Pop());
            }
            return Answer(thirdStep);

        }

        private static string Answer(List<string> myList)
        {
            
            for (int i = 0; i < myList.Count; i++)
            {
                if (myChars.Contains(myList[i]))
                {
                    if (myAct[myList[i]].operands.CompareTo(2) == 0)
                    {
                        myNums.Push(Actions(myNums.Pop(), myNums.Pop(), myList[i]));
                    }
                    else if (myAct[myList[i]].operands.CompareTo(1) == 0)
                    {
                        myNums.Push(Actions(myNums.Pop(), myList[i]));
                    }
                }
                else
                {
                    myNums.Push(double.Parse(myList[i]));
                }
            }
            return myNums.Pop().ToString();
        }

        private static double Actions(double b, double a, string action)
        {
            double myAnswer = 0.0;
            switch (action)
            {
                case "+":
                    myAnswer = a + b;
                    break;
                case "-":
                    myAnswer = a - b;
                    break;
                case "/":
                    myAnswer = a / b;
                    break;
                case "*":
                    myAnswer = a * b;
                    break;
                case "^":
                    myAnswer = Math.Pow(a, b);
                    break;
            }
            return myAnswer;
        }

        private static double Actions(double a, string action)
        {
            double myAnswer = 0.0;
            switch (action)
            {
                case "√":
                    myAnswer = Math.Sqrt(a);
                    break;
            }
            return myAnswer;
        }

        public static string ToMyString(string s)
        {
            StringBuilder newStr = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {

                if (myChars.Contains(s[i].ToString()))
                {
                    if (newStr.Length > 0 && (!(newStr[newStr.Length - 1].CompareTo('@') == 0)))
                    {
                        newStr.Append("@");
                        newStr.Append(s[i]);
                        newStr.Append("@");
                    }
                    else
                    {
                        newStr.Append(s[i]);
                        newStr.Append("@");
                    }
                }
                else
                    newStr.Append(s[i]);
            }
            if (newStr[newStr.Length - 1].ToString().CompareTo("@") == 0)
            {
                newStr.Remove(newStr.Length - 1, 1);
            }
            return ToPoland(newStr.ToString());
        }
    }
}
