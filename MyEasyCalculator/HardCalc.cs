﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyCalculator
{
    public partial class HardCalc : Form
    {
        static List<string> myChars = new List<string>() { "*", "/", "+", "-"};
        StringBuilder a = new StringBuilder("0");
        char[] myNum = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
        public HardCalc()
        {
            InitializeComponent();
        }

        private void ToSimple_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm calc = new MainForm();
            calc.Show();
        }
        private void Append(string s)
        {
            if (a.Length==1 && a[0].CompareTo('0')==0 && !ifAction(s))
            {
                a.Clear();
            }
            if (a.Length==0 && ifAction(s))
            {
                a.Append("0");
            }
            if (s.CompareTo(")") == 0 
                || s.CompareTo("[") == 0
                || s.CompareTo("]") == 0
                || s.CompareTo("^") == 0)
                {
                if (a.Length == 0)
                {
                    a.Append("0");
                    s = string.Empty;
                }
                else if (ifAction(a[a.Length - 1]) || (ifAction(s) && ifAction(a[a.Length - 1])))
                {
                    a.Remove(a.Length - 1, 1);
                }
            }
            if (s.CompareTo(",") == 0)
            {
                if (a.Length == 0)
                {
                    a.Append("0,");  
                }
                if (!IfNum(a[a.Length - 1]))
                {
                    s = string.Empty;
                }
            }
            if (myChars.Contains(s) && myChars.Contains(a[a.Length-1].ToString()))
            {
                a.Remove(a.Length-1,1);
            }
            a.Append(s);
            NumEdit.Text = (a.ToString());
        }
        private bool IfNum(char s)
        {
            for (int i=0; i<myNum.Length; i++)
            {
                if (myNum[i].CompareTo(s) == 0)
                {
                    return true;
                }
            }
            return false;
        }
        private bool ifAction(char s)
        {
            return (
                s.CompareTo('+') == 0 ||
                s.CompareTo('-') == 0 ||
                s.CompareTo('/') == 0 ||
                s.CompareTo('*') == 0);
        }
        private bool ifAction(string s)
        {
            return (
                s.CompareTo("+") == 0 ||
                s.CompareTo("-") == 0 ||
                s.CompareTo("/") == 0 ||
                s.CompareTo("*") == 0);
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            a.Clear();
            a.Append("0");
            NumEdit.Text = (a.ToString());
        }

        private void ButtonBack_Click(object sender, EventArgs e)
        {
            if (a.Length == 0)
            {
                a.Append("0");
            }
            else if (a.Length == 1)
            {
                a.Clear();
                a.Append("0");
            }
            else if (a[a.Length - 2].CompareTo(',') == 0)
            {
                a.Remove(a.Length - 2, 2);
            }
            else
                a.Remove(a.Length - 1, 1);
            NumEdit.Text = (a.ToString());
        }
        private void Button_Click(object sender, EventArgs e)
        {
            Append((sender as Button).Tag as string);
        }
        private void ButtonAnswer_Click(object sender, EventArgs e)
        {
            string answer;
            answer = GetAnswer.ToMyString(a.ToString());
            a.Clear();
            a.Append(answer);
            NumEdit.Text = answer;
            
        }


        private void ButtonOpenPowBracket_Click(object sender, EventArgs e)
        {
            Append((sender as Button).Tag as string);
        }

        private void ButtonClosePowBracket_Click(object sender, EventArgs e)
        {
            Append((sender as Button).Tag as string);
        }

        private void HardCalc_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void NewFormul_Click(object sender, EventArgs e)
        {
            new NewFormul().Show();
        }

        private void MyFormuls_Click(object sender, EventArgs e)
        {

        }
    }
}
