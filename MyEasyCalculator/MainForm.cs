﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyCalculator
{
    public partial class MainForm : Form
    {
        bool ifAction = false;
        StringBuilder a = new StringBuilder();
        StringBuilder b = new StringBuilder();
        string action = string.Empty;
        double answer;

        public MainForm()
        {
            InitializeComponent();
        }

        public void Append(string n)
        {
            if (ifAction)
            {
                if (b.ToString().CompareTo("0") == 0)
                {
                    b.Clear();
                }
                b.Append(n);
                NumEdit.Text = string.Concat(a.ToString(), action, b.ToString());
            }
            else
            {
                if (a.ToString().CompareTo("0") == 0)
                {
                    a.Clear();
                }
                a.Append(n);
                NumEdit.Text = string.Concat(a.ToString(),action,b.ToString());
            }
        }
        private void ButtonClear_Click(object sender, EventArgs e)
        {
                b.Clear();

                a.Clear();
            //a.Append("0");
            NumEdit.Text = "0";
            ifAction = false;
            action = string.Empty;
        }

        private void ButtonBack_Click(object sender, EventArgs e)
        {
            if (ifAction)
            {
                if (NumEdit.Text.Last()=='+' || NumEdit.Text.Last() == '-' || NumEdit.Text.Last() == '/' || NumEdit.Text.Last() == '*')
                {
                    action = string.Empty;
                    ifAction = false;
                    NumEdit.Text = a.ToString();
                }
                else
                {
                    if (b.ToString().CompareTo("-") == 0 || (b.Length == 1))
                    {
                        b.Clear();
                        
                    }
                    else
                        b.Remove(b.Length - 1, 1);
                }
                NumEdit.Text = string.Concat(a.ToString(), action, b.ToString());
            }
            else
            {
                if (a.ToString().CompareTo("-") == 0 || (a.Length == 1))
                {
                    a.Clear();
                    a.Append(0);
                }
                else
                    a.Remove(a.Length - 1, 1);
                NumEdit.Text = string.Concat(a.ToString(), action, b.ToString());
            }
        }

        private void ButtonNum_Click(object sender, EventArgs e)
        {
            Append((sender as Button).Tag as string);
        }
        private void ButtonPlus_Click(object sender, EventArgs e)
        {
            ifAction = true;
            action = ((sender as Button).Tag as string);
            NumEdit.Text = string.Concat(a.ToString(), action, b.ToString());
        }

        private void ButtonMinus_Click(object sender, EventArgs e)
        {
            
            if (a.ToString().CompareTo("") == 0)
            {
                a.Append("-");
            }
            else
            {
                ifAction = true;
                action = ((sender as Button).Tag as string);
            }
            NumEdit.Text = string.Concat(a.ToString(), action, b.ToString());
        }

        private void ButtonMultiply_Click(object sender, EventArgs e)
        {
            NumEdit.Text = String.Empty;
            ifAction = true;
            action = ((sender as Button).Tag as string);
            NumEdit.Text = string.Concat(a.ToString(), action, b.ToString());
        }

        private void ButtonDiv_Click(object sender, EventArgs e)
        {
            NumEdit.Text = String.Empty;
            ifAction = true;
            action = ((sender as Button).Tag as string);
            NumEdit.Text = string.Concat(a.ToString(), action, b.ToString());
        }

        private void ButtoAnswer_Click(object sender, EventArgs e)
        {
            if (!a.ToString().Equals(string.Empty) && !b.ToString().Equals(string.Empty))
            {
                switch (action)
                {
                    case "+":
                        answer = double.Parse(a.ToString()) + double.Parse(b.ToString());
                        NumEdit.Text = answer.ToString();
                        action = string.Empty;
                        break;
                    case "-":
                        answer = double.Parse(a.ToString()) - double.Parse(b.ToString());
                        NumEdit.Text = answer.ToString();
                        action = string.Empty;
                        break;
                    case "*":
                        answer = double.Parse(a.ToString()) * double.Parse(b.ToString());
                        NumEdit.Text = answer.ToString();
                        action = string.Empty;
                        break;
                        
                    case "/":
                        answer = double.Parse(a.ToString()) / double.Parse(b.ToString());
                        NumEdit.Text = answer.ToString();
                        action = string.Empty;
                        break;
                }
            }
            else if (!a.ToString().Equals(string.Empty) && b.ToString().Equals(string.Empty))
            {
                answer = double.Parse(a.ToString());
                NumEdit.Text = answer.ToString();
            }
            else if (a.ToString().Equals(string.Empty) && !b.ToString().Equals(string.Empty))
            {
                answer = double.Parse(b.ToString());
                NumEdit.Text = answer.ToString();
            }
            ifAction = false;
            a.Clear();
            a.Append(answer);
            b.Clear();
        }

        private void ToIngener_Click(object sender, EventArgs e)
        {
            this.Hide();
            HardCalc calc = new HardCalc();
            calc.Show();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
