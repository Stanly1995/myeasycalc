﻿namespace MyEasyCalculator
{
    partial class HardCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonBack = new System.Windows.Forms.Button();
            this.ButtonClear = new System.Windows.Forms.Button();
            this.ButtoAnswer = new System.Windows.Forms.Button();
            this.ButtonDiv = new System.Windows.Forms.Button();
            this.ButtonMultiply = new System.Windows.Forms.Button();
            this.ButtonMinus = new System.Windows.Forms.Button();
            this.ButtonPlus = new System.Windows.Forms.Button();
            this.ButtonDot = new System.Windows.Forms.Button();
            this.ButtonZero = new System.Windows.Forms.Button();
            this.ButtonNine = new System.Windows.Forms.Button();
            this.ButtonEight = new System.Windows.Forms.Button();
            this.ButtonSeven = new System.Windows.Forms.Button();
            this.ButtonSix = new System.Windows.Forms.Button();
            this.ButtonFive = new System.Windows.Forms.Button();
            this.ButtonFour = new System.Windows.Forms.Button();
            this.ButtonThree = new System.Windows.Forms.Button();
            this.ButtonTwo = new System.Windows.Forms.Button();
            this.ButtonOne = new System.Windows.Forms.Button();
            this.NumEdit = new System.Windows.Forms.TextBox();
            this.ButtonSqrt = new System.Windows.Forms.Button();
            this.ButtonPow = new System.Windows.Forms.Button();
            this.ButtonCloseBracket = new System.Windows.Forms.Button();
            this.ButtonOpenBracket = new System.Windows.Forms.Button();
            this.MyMenuStrip = new System.Windows.Forms.MenuStrip();
            this.Theme = new System.Windows.Forms.ToolStripMenuItem();
            this.ToSimple = new System.Windows.Forms.ToolStripMenuItem();
            this.Formuls = new System.Windows.Forms.ToolStripMenuItem();
            this.NewFormul = new System.Windows.Forms.ToolStripMenuItem();
            this.MyFormuls = new System.Windows.Forms.ToolStripMenuItem();
            this.MyMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonBack
            // 
            this.ButtonBack.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonBack.Location = new System.Drawing.Point(295, 104);
            this.ButtonBack.Name = "ButtonBack";
            this.ButtonBack.Size = new System.Drawing.Size(268, 41);
            this.ButtonBack.TabIndex = 38;
            this.ButtonBack.Tag = "back";
            this.ButtonBack.Text = "Back";
            this.ButtonBack.UseVisualStyleBackColor = true;
            this.ButtonBack.Click += new System.EventHandler(this.ButtonBack_Click);
            // 
            // ButtonClear
            // 
            this.ButtonClear.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonClear.Location = new System.Drawing.Point(12, 104);
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.Size = new System.Drawing.Size(256, 41);
            this.ButtonClear.TabIndex = 37;
            this.ButtonClear.Tag = "C";
            this.ButtonClear.Text = "Clear";
            this.ButtonClear.UseVisualStyleBackColor = true;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // ButtoAnswer
            // 
            this.ButtoAnswer.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtoAnswer.Location = new System.Drawing.Point(239, 344);
            this.ButtoAnswer.Name = "ButtoAnswer";
            this.ButtoAnswer.Size = new System.Drawing.Size(88, 41);
            this.ButtoAnswer.TabIndex = 36;
            this.ButtoAnswer.Tag = "=";
            this.ButtoAnswer.Text = "=";
            this.ButtoAnswer.UseVisualStyleBackColor = true;
            this.ButtoAnswer.Click += new System.EventHandler(this.ButtonAnswer_Click);
            // 
            // ButtonDiv
            // 
            this.ButtonDiv.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDiv.Location = new System.Drawing.Point(357, 344);
            this.ButtonDiv.Name = "ButtonDiv";
            this.ButtonDiv.Size = new System.Drawing.Size(88, 41);
            this.ButtonDiv.TabIndex = 35;
            this.ButtonDiv.Tag = "/";
            this.ButtonDiv.Text = "/";
            this.ButtonDiv.UseVisualStyleBackColor = true;
            this.ButtonDiv.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonMultiply
            // 
            this.ButtonMultiply.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonMultiply.Location = new System.Drawing.Point(357, 283);
            this.ButtonMultiply.Name = "ButtonMultiply";
            this.ButtonMultiply.Size = new System.Drawing.Size(88, 41);
            this.ButtonMultiply.TabIndex = 34;
            this.ButtonMultiply.Tag = "*";
            this.ButtonMultiply.Text = "*";
            this.ButtonMultiply.UseVisualStyleBackColor = true;
            this.ButtonMultiply.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonMinus
            // 
            this.ButtonMinus.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonMinus.Location = new System.Drawing.Point(357, 221);
            this.ButtonMinus.Name = "ButtonMinus";
            this.ButtonMinus.Size = new System.Drawing.Size(88, 41);
            this.ButtonMinus.TabIndex = 33;
            this.ButtonMinus.Tag = "-";
            this.ButtonMinus.Text = "-";
            this.ButtonMinus.UseVisualStyleBackColor = true;
            this.ButtonMinus.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonPlus
            // 
            this.ButtonPlus.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonPlus.Location = new System.Drawing.Point(357, 162);
            this.ButtonPlus.Name = "ButtonPlus";
            this.ButtonPlus.Size = new System.Drawing.Size(88, 41);
            this.ButtonPlus.TabIndex = 32;
            this.ButtonPlus.Tag = "+";
            this.ButtonPlus.Text = "+";
            this.ButtonPlus.UseVisualStyleBackColor = true;
            this.ButtonPlus.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonDot
            // 
            this.ButtonDot.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDot.Location = new System.Drawing.Point(12, 344);
            this.ButtonDot.Name = "ButtonDot";
            this.ButtonDot.Size = new System.Drawing.Size(88, 41);
            this.ButtonDot.TabIndex = 31;
            this.ButtonDot.Tag = ",";
            this.ButtonDot.Text = ",";
            this.ButtonDot.UseVisualStyleBackColor = true;
            this.ButtonDot.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonZero
            // 
            this.ButtonZero.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonZero.Location = new System.Drawing.Point(125, 344);
            this.ButtonZero.Name = "ButtonZero";
            this.ButtonZero.Size = new System.Drawing.Size(88, 41);
            this.ButtonZero.TabIndex = 30;
            this.ButtonZero.Tag = "0";
            this.ButtonZero.Text = "0";
            this.ButtonZero.UseVisualStyleBackColor = true;
            this.ButtonZero.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonNine
            // 
            this.ButtonNine.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonNine.Location = new System.Drawing.Point(239, 283);
            this.ButtonNine.Name = "ButtonNine";
            this.ButtonNine.Size = new System.Drawing.Size(88, 41);
            this.ButtonNine.TabIndex = 29;
            this.ButtonNine.Tag = "9";
            this.ButtonNine.Text = "9";
            this.ButtonNine.UseVisualStyleBackColor = true;
            this.ButtonNine.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonEight
            // 
            this.ButtonEight.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonEight.Location = new System.Drawing.Point(125, 283);
            this.ButtonEight.Name = "ButtonEight";
            this.ButtonEight.Size = new System.Drawing.Size(88, 41);
            this.ButtonEight.TabIndex = 28;
            this.ButtonEight.Tag = "8";
            this.ButtonEight.Text = "8";
            this.ButtonEight.UseVisualStyleBackColor = true;
            this.ButtonEight.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonSeven
            // 
            this.ButtonSeven.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonSeven.Location = new System.Drawing.Point(12, 283);
            this.ButtonSeven.Name = "ButtonSeven";
            this.ButtonSeven.Size = new System.Drawing.Size(88, 41);
            this.ButtonSeven.TabIndex = 27;
            this.ButtonSeven.Tag = "7";
            this.ButtonSeven.Text = "7";
            this.ButtonSeven.UseVisualStyleBackColor = true;
            this.ButtonSeven.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonSix
            // 
            this.ButtonSix.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonSix.Location = new System.Drawing.Point(239, 221);
            this.ButtonSix.Name = "ButtonSix";
            this.ButtonSix.Size = new System.Drawing.Size(88, 41);
            this.ButtonSix.TabIndex = 26;
            this.ButtonSix.Tag = "6";
            this.ButtonSix.Text = "6";
            this.ButtonSix.UseVisualStyleBackColor = true;
            this.ButtonSix.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonFive
            // 
            this.ButtonFive.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonFive.Location = new System.Drawing.Point(125, 221);
            this.ButtonFive.Name = "ButtonFive";
            this.ButtonFive.Size = new System.Drawing.Size(88, 41);
            this.ButtonFive.TabIndex = 25;
            this.ButtonFive.Tag = "5";
            this.ButtonFive.Text = "5";
            this.ButtonFive.UseVisualStyleBackColor = true;
            this.ButtonFive.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonFour
            // 
            this.ButtonFour.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonFour.Location = new System.Drawing.Point(12, 221);
            this.ButtonFour.Name = "ButtonFour";
            this.ButtonFour.Size = new System.Drawing.Size(88, 41);
            this.ButtonFour.TabIndex = 24;
            this.ButtonFour.Tag = "4";
            this.ButtonFour.Text = "4";
            this.ButtonFour.UseVisualStyleBackColor = true;
            this.ButtonFour.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonThree
            // 
            this.ButtonThree.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonThree.Location = new System.Drawing.Point(239, 162);
            this.ButtonThree.Name = "ButtonThree";
            this.ButtonThree.Size = new System.Drawing.Size(88, 41);
            this.ButtonThree.TabIndex = 23;
            this.ButtonThree.Tag = "3";
            this.ButtonThree.Text = "3";
            this.ButtonThree.UseVisualStyleBackColor = true;
            this.ButtonThree.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonTwo
            // 
            this.ButtonTwo.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonTwo.Location = new System.Drawing.Point(125, 162);
            this.ButtonTwo.Name = "ButtonTwo";
            this.ButtonTwo.Size = new System.Drawing.Size(88, 41);
            this.ButtonTwo.TabIndex = 22;
            this.ButtonTwo.Tag = "2";
            this.ButtonTwo.Text = "2";
            this.ButtonTwo.UseVisualStyleBackColor = true;
            this.ButtonTwo.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonOne
            // 
            this.ButtonOne.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOne.Location = new System.Drawing.Point(12, 162);
            this.ButtonOne.Name = "ButtonOne";
            this.ButtonOne.Size = new System.Drawing.Size(88, 41);
            this.ButtonOne.TabIndex = 21;
            this.ButtonOne.Tag = "1";
            this.ButtonOne.Text = "1";
            this.ButtonOne.UseVisualStyleBackColor = true;
            this.ButtonOne.Click += new System.EventHandler(this.Button_Click);
            // 
            // NumEdit
            // 
            this.NumEdit.Enabled = false;
            this.NumEdit.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumEdit.Location = new System.Drawing.Point(12, 47);
            this.NumEdit.Name = "NumEdit";
            this.NumEdit.Size = new System.Drawing.Size(551, 38);
            this.NumEdit.TabIndex = 20;
            this.NumEdit.Text = "0";
            this.NumEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ButtonSqrt
            // 
            this.ButtonSqrt.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonSqrt.Location = new System.Drawing.Point(475, 344);
            this.ButtonSqrt.Name = "ButtonSqrt";
            this.ButtonSqrt.Size = new System.Drawing.Size(88, 41);
            this.ButtonSqrt.TabIndex = 42;
            this.ButtonSqrt.Tag = "√";
            this.ButtonSqrt.Text = "√";
            this.ButtonSqrt.UseVisualStyleBackColor = true;
            this.ButtonSqrt.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonPow
            // 
            this.ButtonPow.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonPow.Location = new System.Drawing.Point(475, 283);
            this.ButtonPow.Name = "ButtonPow";
            this.ButtonPow.Size = new System.Drawing.Size(88, 41);
            this.ButtonPow.TabIndex = 41;
            this.ButtonPow.Tag = "^";
            this.ButtonPow.Text = "^";
            this.ButtonPow.UseVisualStyleBackColor = true;
            this.ButtonPow.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonCloseBracket
            // 
            this.ButtonCloseBracket.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonCloseBracket.Location = new System.Drawing.Point(475, 221);
            this.ButtonCloseBracket.Name = "ButtonCloseBracket";
            this.ButtonCloseBracket.Size = new System.Drawing.Size(88, 41);
            this.ButtonCloseBracket.TabIndex = 40;
            this.ButtonCloseBracket.Tag = ")";
            this.ButtonCloseBracket.Text = ")";
            this.ButtonCloseBracket.UseVisualStyleBackColor = true;
            this.ButtonCloseBracket.Click += new System.EventHandler(this.Button_Click);
            // 
            // ButtonOpenBracket
            // 
            this.ButtonOpenBracket.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOpenBracket.Location = new System.Drawing.Point(475, 162);
            this.ButtonOpenBracket.Name = "ButtonOpenBracket";
            this.ButtonOpenBracket.Size = new System.Drawing.Size(88, 41);
            this.ButtonOpenBracket.TabIndex = 39;
            this.ButtonOpenBracket.Tag = "(";
            this.ButtonOpenBracket.Text = "(";
            this.ButtonOpenBracket.UseVisualStyleBackColor = true;
            this.ButtonOpenBracket.Click += new System.EventHandler(this.Button_Click);
            // 
            // MyMenuStrip
            // 
            this.MyMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MyMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Theme,
            this.Formuls});
            this.MyMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.MyMenuStrip.Name = "MyMenuStrip";
            this.MyMenuStrip.Size = new System.Drawing.Size(573, 28);
            this.MyMenuStrip.TabIndex = 47;
            this.MyMenuStrip.Text = "menuStrip1";
            // 
            // Theme
            // 
            this.Theme.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToSimple});
            this.Theme.Name = "Theme";
            this.Theme.Size = new System.Drawing.Size(47, 24);
            this.Theme.Text = "Вид";
            // 
            // ToSimple
            // 
            this.ToSimple.Name = "ToSimple";
            this.ToSimple.Size = new System.Drawing.Size(216, 26);
            this.ToSimple.Text = "Простой";
            // 
            // Formuls
            // 
            this.Formuls.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewFormul,
            this.MyFormuls});
            this.Formuls.Name = "Formuls";
            this.Formuls.Size = new System.Drawing.Size(87, 24);
            this.Formuls.Text = "Формулы";
            // 
            // NewFormul
            // 
            this.NewFormul.Name = "NewFormul";
            this.NewFormul.Size = new System.Drawing.Size(216, 26);
            this.NewFormul.Text = "Добавить";
            this.NewFormul.Click += new System.EventHandler(this.NewFormul_Click);
            // 
            // MyFormuls
            // 
            this.MyFormuls.Name = "MyFormuls";
            this.MyFormuls.Size = new System.Drawing.Size(216, 26);
            this.MyFormuls.Text = "Просмотреть";
            this.MyFormuls.Click += new System.EventHandler(this.MyFormuls_Click);
            // 
            // HardCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 396);
            this.Controls.Add(this.MyMenuStrip);
            this.Controls.Add(this.ButtonSqrt);
            this.Controls.Add(this.ButtonPow);
            this.Controls.Add(this.ButtonCloseBracket);
            this.Controls.Add(this.ButtonOpenBracket);
            this.Controls.Add(this.ButtonBack);
            this.Controls.Add(this.ButtonClear);
            this.Controls.Add(this.ButtoAnswer);
            this.Controls.Add(this.ButtonDiv);
            this.Controls.Add(this.ButtonMultiply);
            this.Controls.Add(this.ButtonMinus);
            this.Controls.Add(this.ButtonPlus);
            this.Controls.Add(this.ButtonDot);
            this.Controls.Add(this.ButtonZero);
            this.Controls.Add(this.ButtonNine);
            this.Controls.Add(this.ButtonEight);
            this.Controls.Add(this.ButtonSeven);
            this.Controls.Add(this.ButtonSix);
            this.Controls.Add(this.ButtonFive);
            this.Controls.Add(this.ButtonFour);
            this.Controls.Add(this.ButtonThree);
            this.Controls.Add(this.ButtonTwo);
            this.Controls.Add(this.ButtonOne);
            this.Controls.Add(this.NumEdit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "HardCalc";
            this.Text = "HardCalc";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HardCalc_FormClosing);
            this.MyMenuStrip.ResumeLayout(false);
            this.MyMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonBack;
        private System.Windows.Forms.Button ButtonClear;
        private System.Windows.Forms.Button ButtoAnswer;
        private System.Windows.Forms.Button ButtonDiv;
        private System.Windows.Forms.Button ButtonMultiply;
        private System.Windows.Forms.Button ButtonMinus;
        private System.Windows.Forms.Button ButtonPlus;
        private System.Windows.Forms.Button ButtonDot;
        private System.Windows.Forms.Button ButtonZero;
        private System.Windows.Forms.Button ButtonNine;
        private System.Windows.Forms.Button ButtonEight;
        private System.Windows.Forms.Button ButtonSeven;
        private System.Windows.Forms.Button ButtonSix;
        private System.Windows.Forms.Button ButtonFive;
        private System.Windows.Forms.Button ButtonFour;
        private System.Windows.Forms.Button ButtonThree;
        private System.Windows.Forms.Button ButtonTwo;
        private System.Windows.Forms.Button ButtonOne;
        private System.Windows.Forms.TextBox NumEdit;
        private System.Windows.Forms.Button ButtonSqrt;
        private System.Windows.Forms.Button ButtonPow;
        private System.Windows.Forms.Button ButtonCloseBracket;
        private System.Windows.Forms.Button ButtonOpenBracket;
        private System.Windows.Forms.MenuStrip MyMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem Theme;
        private System.Windows.Forms.ToolStripMenuItem ToSimple;
        private System.Windows.Forms.ToolStripMenuItem Formuls;
        private System.Windows.Forms.ToolStripMenuItem NewFormul;
        private System.Windows.Forms.ToolStripMenuItem MyFormuls;
    }
}